const { EmbedBuilder } = require('discord.js');
module.exports = async({ client, inter, queue }) => {
    if (!queue || !queue.playing) return inter.reply({ content: `Сейчас не играет музыка ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

    const track = queue.current;

    const methods = ['Отключено', 'Трек', 'Очередь'];

    const timestamp = queue.getPlayerTimestamp();

    const trackDuration = timestamp.progress == 'Infinity' ? 'infinity (live)' : track.duration;

    const progress = queue.createProgressBar();


    const embed = new EmbedBuilder()
        .setAuthor({ name: track.title, iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true }) })
        .setThumbnail(track.thumbnail)
        .setDescription(`Громкость **${queue.volume}**%\nДлинна **${trackDuration}**\nПроцесc ${progress}\nЗацикливание **${methods[queue.repeatMode]}**\nЗапрошено ${track.requestedBy}`)
        .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })
        .setColor('ff0000')
        .setTimestamp()

    inter.reply({ embeds: [embed], ephemeral: true });
}