module.exports = async({ inter, queue }) => {
        if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится... повторить попытку? ❌`, ephemeral: true });

        const success = queue.setPaused(false);

        if (!success) queue.setPaused(true);


        return inter.reply({ content: `${success ? `Текущая музыка ${queue.current.title} приостановлена ✅` : `Текущая музыка ${queue.current.title} возобновлена ✅`}`, ephemeral: true});
}