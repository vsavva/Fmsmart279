const { EmbedBuilder } = require('discord.js');
module.exports = async({ client, inter, queue }) => {
    if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится... повторить попытку? ❌`, ephemeral: true });

    if (!queue.tracks[0]) return inter.reply({ content: `Нет музыки в очереди после текущей ${inter.member}... попробовать еще раз? ❌`, ephemeral: true });

    const methods = ['', '🔁', '🔂'];

    const songs = queue.tracks.length;

    const nextSongs = songs > 5 ? `И **${songs - 5}** другие песни...` : `В плейлисте **${songs}** песни...`;

    const tracks = queue.tracks.map((track, i) => `**${i + 1}** - ${track.title} | ${track.author} (Запрошенный : ${track.requestedBy.username})`)

    const embed = new EmbedBuilder()
        .setColor('#ff0000')
        .setThumbnail(inter.guild.iconURL({ size: 2048, dynamic: true }))
        .setAuthor({ name: `Очередь сервера - ${inter.guild.name} ${methods[queue.repeatMode]}`, iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true }) })
        .setDescription(`Current ${queue.current.title}\n\n${tracks.slice(0, 5).join('\n')}\n\n${nextSongs}`)
        .setTimestamp()
        .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })

    inter.reply({ embeds: [embed], ephemeral: true });
}