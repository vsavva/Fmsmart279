module.exports = async({ inter, queue }) => {
    if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится... повторить попытку? ❌`, ephemeral: true });

    const success = queue.skip();

    return inter.reply({ content: success ? `Текущая музыка ${queue.current.title} пропущена ✅` : `Что-то пошло не так ${inter.member}... попробовать еще раз? ❌`, ephemeral: true });
}