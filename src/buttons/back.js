module.exports = async({ inter, queue }) => {
    if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится... повторить попытку? ❌`, ephemeral: true });

    if (!queue.previousTracks[1]) return inter.reply({ content: `До ${inter.member} музыка не проигрывалась... попробовать еще раз ? ❌`, ephemeral: true });

    await queue.back();

    inter.reply({ content: `Воспроизведение **предыдущего** трека ✅`, ephemeral: true });
}