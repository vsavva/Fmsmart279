const { QueueRepeatMode } = require('discord-player');
module.exports = async({ inter, queue }) => {

    const methods = ['Выключено', 'Трек', 'Очередь'];

    if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится... повторить попытку? ❌`, ephemeral: true });

    const repeatMode = queue.repeatMode

    if (repeatMode === 0) queue.setRepeatMode(QueueRepeatMode.TRACK)

    if (repeatMode === 1) queue.setRepeatMode(QueueRepeatMode.QUEUE)

    if (repeatMode === 2) queue.setRepeatMode(QueueRepeatMode.OFF)

    return inter.reply({ content: `Созданный цикл был установлен на **${methods[queue.repeatMode]}**.✅` })



}