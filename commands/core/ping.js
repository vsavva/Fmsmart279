const ms = require('ms');

module.exports = {
    name: 'ping',
    description: "Получить пинг бота!",
    async execute({ client, inter }) {

        const m = await inter.reply("Пинг?")
        inter.editReply(`Понг! Задержка составляет ${Math.round(client.ws.ping)} мс 🛰️`)

    },
};