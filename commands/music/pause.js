module.exports = {
    name: 'pause',
    description: 'приостановить трек',
    voiceChannel: true,

    execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue) return inter.reply({ content: `Сейчас не играет музыка ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

        if (queue.connection.paused) return inter.reply({ content: 'Трек остановлен', ephemeral: true })

        if (queue.connection.paused) return inter.reply({ content: `Трек в настоящее время приостановлен, ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true })

        const success = queue.setPaused(true);

        return inter.reply({ content: success ? `Текущая музыка ${queue.current.title} приостановлена ✅` : `Что-то пошло не так ${inter.member}... попробуйте еще раз ? ❌` });
    },
};