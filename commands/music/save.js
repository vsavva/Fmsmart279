const { EmbedBuilder } = require("discord.js");

module.exports = {
    name: 'save',
    description: 'сохранить текущий трек!',
    voiceChannel: true,

    async execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue) return inter.reply({ content: `В настоящее время ${inter.member} не воспроизводит музыку... повторить попытку? ❌`, ephemeral: true });

        inter.member.send({
            embeds: [
                new EmbedBuilder()
                .setColor('Red')
                .setTitle(`:arrow_forward: ${queue.current.title}`)
                .setURL(queue.current.url)
                .addFields({ name: ':hourglass: Продолжительность:', value: `\`${queue.current.duration}\``, inline: true }, { name: 'Автор:', value: `\`${queue.current.author}\``, inline: true }, { name: 'Просмотры :eyes:', value: `\`${Number(queue.current.views).toLocaleString()}\``, inline: true }, { name: 'Ссылка:', value: `\`${queue.current.url}\`` })
                .setThumbnail(queue.current.thumbnail)
                .setFooter({ text: `с сервера ${inter.member.guild.name}`, iconURL: inter.member.guild.iconURL({ dynamic: false }) })
            ]
        }).then(() => {
            return inter.reply({ content: `Я отправила вам название музыки в личные сообщения ✅`, ephemeral: true });
        }).catch(error => {
            return inter.reply({ content: `Не удалось отправить вам личное сообщение... попробуйте еще раз? ❌`, ephemeral: true });
        });
    },
};