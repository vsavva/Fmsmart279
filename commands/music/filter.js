const { ApplicationCommandOptionType } = require('discord.js');

module.exports = {
        name: 'filter',
        description: 'Добавить фильтер на музыку',
        voiceChannel: true,
        options: [{
            name: 'filter',
            description: 'Фильтер который вы хотите вы хотите добавить',
            type: ApplicationCommandOptionType.String,
            required: true,
            choices: [...Object.keys(require("discord-player").AudioFilters.filters).map(m => Object({ name: m, value: m })).splice(0, 25)],
        }],


        async execute({ inter, client }) {
            const queue = player.getQueue(inter.guildId);

            if (!queue || !queue.playing) return inter.reply({ content: `Сейчас не играет музыка ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

            const actualFilter = queue.getFiltersEnabled()[0];

            const infilter = inter.options.getString('filter');


            const filters = [];

            queue.getFiltersEnabled().map(x => filters.push(x));
            queue.getFiltersDisabled().map(x => filters.push(x));

            const filter = filters.find((x) => x.toLowerCase() === infilter.toLowerCase());

            if (!filter) return inter.reply({ content: `Этого фильтра не существуют ${inter.member}... попробуйте еще раз ? ❌\n${actualFilter ? `Фильтр в настоящее время активен ${actualFilter}.\n` : ''}Список доступных фильтров ${filters.map(x => `**${x}**`).join(', ')}.`, ephemeral: true });

        const filtersUpdated = {};

        filtersUpdated[filter] = queue.getFiltersEnabled().includes(filter) ? false : true;

        await queue.setFilters(filtersUpdated);

        inter.reply({ content: `Фильтр ${filter} **${queue.getFiltersEnabled().includes(filter) ? 'включен' : 'отключен'}** ✅\n*Напоминаю, чем длиннее музыка, тем больше времени это займет.*` });
    },
};