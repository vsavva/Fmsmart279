module.exports = {
    name: 'shuffle',
    description: 'перетасовать трек',
    voiceChannel: true,

    async execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время ${inter.member} не воспроизводит музыку... повторить попытку? ❌`, ephemeral: true });

        if (!queue.tracks[0]) return inter.reply({ content: `Нет музыки в очереди после текущей ${inter.member}... попробовать еще раз? ❌`, ephemeral: true });

        await queue.shuffle();

        return inter.reply({ content: `В очереди смешаны **${queue.tracks.length}** песни(и) ! ✅` });
    },
};