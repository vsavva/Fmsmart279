module.exports = {
    name: 'skip',
    description: 'остановить трек',
    voiceChannel: true,

    execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время ${inter.member} не воспроизводит музыку... повторить попытку? ❌`, ephemeral: true });

        const success = queue.skip();

        return inter.reply({ content: success ? `Текущая музыка ${queue.current.title} пропущена ✅` : `Что-то пошло не так ${inter.member}... попробовать еще раз? ❌` });
    },
};