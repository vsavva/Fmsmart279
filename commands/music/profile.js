const { EmbedBuilder } = require('discord.js');

module.exports = {
    name: 'profile',
    description: 'Профиль',
    voiceChannel: true,
    execute({ inter }) {
        const embed = new EmbedBuilder()
            .setThumbnail("да")
            .setDescription(`Мазелов умер профиля не будет`)
            .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })
            .setColor('ff0000')
            .setTimestamp()
        inter.reply({ embeds: [embed] });
    }
};