module.exports = {
    name: 'resume',
    description: 'воспроизвести трек',
    voiceChannel: true,

    execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue) return inter.reply({ content: `В настоящее время ${inter.member} не воспроизводит музыку... повторить попытку? ❌`, ephemeral: true });


        if (!queue.connection.paused) return inter.reply({ content: `Трек уже запущен, ${inter.member}... попробовать еще раз ? ❌`, ephemeral: true })

        const success = queue.setPaused(false);

        return inter.reply({ content: success ? `Текущая музыка ${queue.current.title} возобновлена ​​✅` : `Что-то пошло не так ${inter.member}... попробовать еще раз? ❌` });
    },
};