module.exports = {
    name: 'back',
    description: "Включить предыдущею песню",
    voiceChannel: true,

    async execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится ${inter.member}... попробуйте снова ? ❌`, ephemeral: true });

        if (!queue.previousTracks[1]) return inter.reply({ content: `До ${inter.member} музыка не проигрывалась... попробуйте снова ? ❌`, ephemeral: true });

        await queue.back();

        inter.reply({ content: `Воспроизведение **предыдущего** трека ✅` });
    },
};