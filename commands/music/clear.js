module.exports = {
    name: 'clear',
    description: 'Очистить музыку',
    voiceChannel: true,

    async execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue || !queue.playing) return inter.reply({ content: `В настоящее время музыка не воспроизводится ${inter.member}... попробуйте снова ? ❌`, ephemeral: true });

        if (!queue.tracks[0]) return inter.reply({ content: `Нет музыки в очереди после текущей ${inter.member}... попробовать еще раз? ❌`, ephemeral: true });

        await queue.clear();

        inter.reply(`Очередь была очищена 🗑️`);
    },
};