const maxVol = client.config.opt.maxVol;
const { ApplicationCommandOptionType } = require('discord.js');

module.exports = {
    name: 'volume',
    description: 'Регулировать громкость',
    voiceChannel: true,
    options: [{
        name: 'volume',
        description: 'Громкость',
        type: ApplicationCommandOptionType.Number,
        required: true,
        minValue: 1,
        maxValue: maxVol
    }],

    execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue) return inter.reply({ content: `В настоящее время ${inter.member} не воспроизводит музыку... повторить попытку? ❌`, ephemeral: true });
        const vol = inter.options.getNumber('volume')

        if (queue.volume === vol) return inter.reply({ content: `Том, который вы хотите изменить, уже является текущим ${inter.member}... попробуйте еще раз? ❌`, ephemeral: true });

        const success = queue.setVolume(vol);

        return inter.reply({ content: success ? `Громкость была изменена на **${vol}**/**${maxVol}**% 🔊` : `Что-то пошло не так ${inter.member}... попробовать еще раз? ❌` });
    },
};