const { EmbedBuilder } = require('discord.js');

module.exports = {
    name: 'queue',
    description: 'Получить песни в очереди',
    voiceChannel: true,

    execute({ client, inter }) {
        const queue = player.getQueue(inter.guildId);


        if (!queue) return inter.reply({ content: `Сейчас не играет музыка ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

        if (!queue.tracks[1]) return inter.reply({ content: `Нет музыки в очереди после текущей ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

        const methods = ['', '🔁', '🔂'];

        const songs = queue.tracks.length;

        const nextSongs = songs > 5 ? `И **${songs - 5}** другие песня(и)...` : `В плейлисте **${songs}** песня(и)...`;

        const tracks = [`**0** - ${queue.current.title} | ${queue.current.author} (запрошенный : ${queue.current.requestedBy.username})`].concat(queue.tracks.map((track, i) => `**${i + 1}** - ${track.title} | ${track.author} (запрошенный : ${track.requestedBy.username})`))

        const embed = new EmbedBuilder()
            .setColor('#ff0000')
            .setThumbnail(inter.guild.iconURL({ size: 2048, dynamic: true }))
            .setAuthor({ name: `Очередь сервера - ${inter.guild.name} ${methods[queue.repeatMode]}`, iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true }) })
            .setDescription(`${tracks.slice(0, 5).join('\n')}\n\n${nextSongs}`)
            .setTimestamp()
            .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })

        inter.reply({ embeds: [embed] });

    },
};