const { EmbedBuilder, ActionRowBuilder, ButtonBuilder } = require('discord.js');

module.exports = {
    name: 'nowplaying',
    description: 'посмотри что играет!',
    voiceChannel: true,

    execute({ inter }) {
        const queue = player.getQueue(inter.guildId);

        if (!queue) return inter.reply({ content: `Сейчас не играет музыка ${inter.member}... попробуйте еще раз ? ❌`, ephemeral: true });

        const track = queue.current;

        const methods = ['Отключено', 'Трек', 'Очередь'];

        const timestamp = queue.getPlayerTimestamp();

        const trackDuration = timestamp.progress == 'Infinity' ? 'infinity (live)' : track.duration;

        const progress = queue.createProgressBar();


        const embed = new EmbedBuilder()
            .setAuthor({ name: track.title, iconURL: client.user.displayAvatarURL({ size: 1024, dynamic: true }) })
            .setThumbnail(track.thumbnail)
            .setDescription(`Громкость **${queue.volume}**%\nДлинна **${trackDuration}**\nПроцесc ${progress}\nЗацикливание **${methods[queue.repeatMode]}**\nЗапрошено ${track.requestedBy}`)
            .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })
            .setColor('ff0000')
            .setTimestamp()

        const saveButton = new ButtonBuilder()
            .setLabel('Сохранить этот трек')
            .setCustomId(JSON.stringify({ ffb: 'savetrack' }))
            .setStyle('Danger')

        const volumeup = new ButtonBuilder()
            .setLabel('Повысить громкость ')
            .setCustomId(JSON.stringify({ ffb: 'volumeup' }))
            .setStyle('Primary')

        const volumedown = new ButtonBuilder()
            .setLabel('Понизить громкость')
            .setCustomId(JSON.stringify({ ffb: 'volumedown' }))
            .setStyle('Primary')

        const loop = new ButtonBuilder()
            .setLabel('Зациклить')
            .setCustomId(JSON.stringify({ ffb: 'loop' }))
            .setStyle('Danger')

        const resumepause = new ButtonBuilder()
            .setLabel('Продолжить & Пауза')
            .setCustomId(JSON.stringify({ ffb: 'resume&pause' }))
            .setStyle('Success')



        const row = new ActionRowBuilder().addComponents(volumedown, saveButton, resumepause, loop, volumeup);

        inter.reply({ embeds: [embed], components: [row] });
    },
};