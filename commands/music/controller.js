const { ApplicationCommandOptionType, ActionRowBuilder, ButtonBuilder, EmbedBuilder, PermissionsBitField } = require('discord.js');

module.exports = {
    name: 'controller',
    description: "Призвать панель управления ",
    voiceChannel: false,
    permissions: PermissionsBitField.Flags.ManageMessages,
    options: [{
        name: 'channel',
        description: 'Канал куда отправить ',
        type: ApplicationCommandOptionType.Channel,
        required: true,
    }],
    async execute({ inter, client }) {
        let Channel = inter.options.getChannel('channel');
        if (Channel.type !== 0) return inter.reply({ content: `Вы должны отправить его в текстовый канал .. ❌`, ephemeral: true })


        const embed = new EmbedBuilder()
            .setTitle('Панель управления')
            .setImage(inter.guild.iconURL({ size: 4096, dynamic: true }))
            .setColor('#36393e')
            .setFooter({ text: 'Fmsmart ❤️', iconURL: inter.member.avatarURL({ dynamic: true }) })


        inter.reply({ content: `Отправляю панель управления в ${Channel}... ✅`, ephemeral: true })

        const back = new ButtonBuilder()
            .setLabel('Назад')
            .setCustomId(JSON.stringify({ ffb: 'back' }))
            .setStyle('Primary')

        const skip = new ButtonBuilder()
            .setLabel('Пропустить')
            .setCustomId(JSON.stringify({ ffb: 'skip' }))
            .setStyle('Primary')

        const resumepause = new ButtonBuilder()
            .setLabel('Возобновить & Пауза')
            .setCustomId(JSON.stringify({ ffb: 'resume&pause' }))
            .setStyle('Danger')

        const save = new ButtonBuilder()
            .setLabel('Сохранить')
            .setCustomId(JSON.stringify({ ffb: 'savetrack' }))
            .setStyle('Success')

        const volumeup = new ButtonBuilder()
            .setLabel('Прибавть звук')
            .setCustomId(JSON.stringify({ ffb: 'volumeup' }))
            .setStyle('Primary')

        const volumedown = new ButtonBuilder()
            .setLabel('Убавить звук')
            .setCustomId(JSON.stringify({ ffb: 'volumedown' }))
            .setStyle('Primary')

        const loop = new ButtonBuilder()
            .setLabel('Зациклить')
            .setCustomId(JSON.stringify({ ffb: 'loop' }))
            .setStyle('Danger')

        const np = new ButtonBuilder()
            .setLabel('Сейчас играет')
            .setCustomId(JSON.stringify({ ffb: 'nowplaying' }))
            .setStyle('Secondary')

        const queuebutton = new ButtonBuilder()
            .setLabel('Очередь')
            .setCustomId(JSON.stringify({ ffb: 'queue' }))
            .setStyle('Secondary')


        const row1 = new ActionRowBuilder().addComponents(back, queuebutton, resumepause, np, skip)
        const row2 = new ActionRowBuilder().addComponents(volumedown, loop, save, volumeup)



        Channel.send({ embeds: [embed], components: [row1, row2] })

    },
}